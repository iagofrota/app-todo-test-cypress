// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

import "cypress-localstorage-commands"

Cypress.Commands.add('addTodo', (tarefa) => {
    cy.get('[placeholder="Nova tarefa"]').type(tarefa)
    cy.get('.btn').click()
})

Cypress.Commands.add('concluirTarefaPeloNome', (tarefa) => {
    cy.contains('.task-grid>.task.pending', tarefa).should('be.visible')
    cy.contains('.task-grid>.task.pending', tarefa).click()
})
