![https://todo.iagofrota.com.br/](todo.iagofrota.com.br_.png)

## Introdução
Projeto para testar o app [TODO](https://todo.iagofrota.com.br/) que fiz a um tempo atrás. A motivação para esse 
fazer projeto para teste, é colocar em prática os diversos conceitos aprendidos nos cursos que fiz e estou fazendo sobre 
[Cypress](https://docs.cypress.io/). Tenho a motivação de adicionar outras melhorias durante todo o percurso que 
estou fazendo dentro do Cypress, inclusive aplicar integração contínua com Gitlab CI.

## O que será testado?
- [ ] CRUD de tarefas
  - [ ] Tentar adicionar tarefa repetidas
  - [ ] Tentar adicionar tarefas vazias
  - [ ] Concluir e tornar pendente uma tarefa novamente
  - [ ] Excluir uma tarefa
- [ ] Manipulação do LocalStorage
- [ ] Leitura de arquivos para fazer a inserção das tarefas
- [ ] Validação da porcentagem das tarefas concluídas
- [ ] Validação dos estilos aplicados quando uma tarefa é concluída

## Instalação
1. Clone o projeto na sua máquina 
   
```bash 
   git clone https://gitlab.com/iagofrota/app-todo-test-cypress.git
```

2. Execute o comando para instalar os pacotes
   
```bash
   npm install
```

3. Por fim, execute e seja feliz

```bash 
   npm run cypress:open
```
