/// <reference types="cypress" />

beforeEach(() => cy.visit(Cypress.config('baseUrl')))

const tarefa = 'Ler Ortodoxia'

describe('TODO', () => {
    it('Verificar o título principal da página', () => {
        cy.get('h1').contains('Tarefas').should('be.visible')
    })

    it('Adicionar uma tarefa', () => {
        cy.addTodo(tarefa)

        cy.contains('.task-grid>.task.pending', tarefa).should('be.visible')
    })

    it('Concluir tarefa', () => {
        cy.addTodo(tarefa)

        cy.concluirTarefaPeloNome(tarefa)

        cy.contains('.task-grid>.task.done', tarefa).should('be.visible')
    })

    it('Excluir tarefa', () => {
        cy.addTodo(tarefa)

        cy.contains('.task-grid>.task.pending', tarefa).should('be.visible')

        cy.get(".task-grid>.task.pending>span[class='close']").click()

        cy.contains("p[class='no-task']", 'Sua vida está em dia =)').should('be.visible')
    })

    it('Concluir e desconcluir uma tarefa', () => {
        cy.addTodo(tarefa)

        cy.concluirTarefaPeloNome(tarefa)

        cy.contains('.task-grid>.task.done', tarefa).should('be.visible')
        cy.contains('.task-grid>.task.done', tarefa).click()

        cy.contains('.task-grid>.task.pending', tarefa).should('be.visible')
    })

    it('Validar porcentagem de conclusão', () => {
        const tarefas = [
            'Tarefas 1',
            'Tarefas 2',
            'Tarefas 3',
        ]

        tarefas.forEach((tarefa) => {
            cy.addTodo(tarefa)
        })

        cy.get("span[class='progress-value']:contains(0%)").should('be.visible')

        cy.concluirTarefaPeloNome(tarefas[1])

        cy.get("span[class='progress-value']:contains(33%)").should('be.visible')

        cy.concluirTarefaPeloNome(tarefas[0])
        cy.concluirTarefaPeloNome(tarefas[2])

        cy.get("span[class='progress-value']:contains(100%)").should('be.visible')
    })
})
